ipfs.svg

- Website: https://github.com/simple-icons/simple-icons/blob/9.19.0/icons/ipfs.svg
- License: CC0 1.0 Universal

lightning.svg

- Website: https://commons.wikimedia.org/wiki/File:Bitcoin_lightning_logo.svg
- License: CC BY-SA 4.0
