export const ENV = import.meta.env.NODE_ENV

export const BACKEND_URL = import.meta.env.VITE_BACKEND_URL
export const APP_NAME = "mitra-web"
export const APP_VERSION = import.meta.env.APP_VERSION
